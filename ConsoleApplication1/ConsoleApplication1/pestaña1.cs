﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class pestaña1
    {
        public static void usarwhile ()
        {
            decimal a, b;
            Console.WriteLine("Usaremos La Estructura While\n");
            Console.WriteLine("Escriba Un Numero\n");
            a = Decimal.Parse(Console.ReadLine());
            Console.WriteLine("Escriba Un Numero Mayor Al Que Ya Escribio\n");
            b = Decimal.Parse(Console.ReadLine());
            while (b > a)
            {
                a = b;
                Console.WriteLine("Escriba Un Numero Mayor\n");
                b= Decimal.Parse(Console.ReadLine());
            }
            if (b <= a )
            {
                Console.WriteLine("No Puedes Ingresar Un Numero Menor O Igual Al Primero\n");
                Console.WriteLine("Presiona Una Tecla Para Salir\n");
            }
            Console.ReadKey();
        }

    }
}
