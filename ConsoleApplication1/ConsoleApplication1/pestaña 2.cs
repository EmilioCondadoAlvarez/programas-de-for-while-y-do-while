﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class pestaña_2
    {
        public static void usarfor ()
        {
            int n, i, tabla;
            Console.WriteLine("Usaremos La Estructura For\n");
            Console.WriteLine("Ingrese Un Numero Para Obtener Su Tabla\n");
            n= Int32.Parse(Console.ReadLine());
            for (i = 1; i <= 10; i++)
            {
                tabla = (n * i);
                Console.WriteLine("La tabla del\t" + n + "\tpor\t" + i + "\tes\t" + tabla + "\n" );   
            }
            Console.WriteLine("Presione Una Tecla Para Salir\n");
            Console.ReadKey();
        }
    }
}
