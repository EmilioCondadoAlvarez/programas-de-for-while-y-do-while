﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion;
            Console.WriteLine("\nEscriba un numero entre 1 y 3\n");
            opcion = int.Parse(Console.ReadLine());
            switch (opcion)
            {
                case 1:
                    pestaña1.usarwhile();
                    break;
                case 2:
                    pestaña_2.usarfor();
                    break;
                case 3:
                    pestaña3.usardowhile();
                    break;

            }

        }
    }
}
